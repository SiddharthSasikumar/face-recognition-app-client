# Library Imports
from flask import Flask, render_template, request, session, redirect, url_for
from forms import LoginForm
from flask import request
from flask import jsonify
import mysql.connector
import os
import jwt


# Database Configuration.
USER = 'root'
PASSWORD = 'soupu007'
HOST = '127.0.0.1'
DATABASE = 'frapp'

# Establishing connection with database.
connection = mysql.connector.connect(
    user=USER,
    password=PASSWORD,
    host=HOST,
    database=DATABASE)


if(connection):
    print('Connection with database established.')
    # Establishing cursor object.
    cursor = connection.cursor(buffered=True)

# Creating Flask app instance.
app = Flask(__name__)

# Configuring app with a secret key.
SECRET_KEY = 'secret'
app.config['SECRET_KEY'] = SECRET_KEY
print(SECRET_KEY)


# ------------------------------------------#
# App Routes
# ------------------------------------------#
@app.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if request.method == "POST":

        if(form.validate() == False):
            print(form.validate())
            return render_template("login.html", form=form)
        else:
            email = form.email.data
            pswd = form.password.data
            print("EMAIL ---- >" + email)
            query = (
                "SELECT password FROM users WHERE email = '{}'".format(email))
            print(query)
            cursor.execute(query)

            for (password) in cursor:
                print("{}".format(password))
                if(pswd == password[0]):
                    print("Password Match!")
                    session['email'] = form.email.data
                    return redirect(url_for('home'))
                else:
                    print("Password Incorrect!")
                    return redirect(url_for('login'))

    #
    #     return redirect(url_for('home'))

    elif(request.method == "GET"):
        return render_template("login.html", form=form)

# Logout route.
@app.route('/logout')
def logout():
    session.pop('email', None)
    return redirect(url_for('login'))

# Home route.
@app.route('/home')
def home():
    if 'email' not in session:
        return redirect(url_for('login'))
    return render_template("home.html")


# Post route.
@app.route('/post', methods=['GET', 'POST'])
def post():
    if request.method == "POST":
        encoded_jwt = request.args.get('encoded_jwt', '')

        try:
            auth = jwt.decode(encoded_jwt, 'secret', algorithms=['HS256'])
            resp = jsonify(success=True)
            return resp, 200
        except:
            resp = jsonify(success=False)
            return resp, 403


# Starting app.
if __name__ == "__main__":
    app.run(debug=True)
