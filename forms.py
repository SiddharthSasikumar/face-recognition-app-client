# Library Imports
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email


# Login Form
class LoginForm(FlaskForm):
    email = StringField('Email')
    password = PasswordField('Password',
                             validators=[DataRequired("Please enter your password")])
    submit = SubmitField('Sign in')
